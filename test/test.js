var repl = require("repl");

var ut = require('../util.js');

var a = {
    x : {
        y : "afasdfas",
        k : 12,
        "z" : [1, 2, 4, 6, 7, 8]
    },
    y : 12321,
    z : "fdasdff"
};

var a1 = ut.merge(a, {
    x : {
        k : 222,
        z : [3, 4]
    },
    f : "fasdf"
});
// console.log('a1:', a1);


var r = repl.start({
    prompt: ">> ",
    input: process.stdin,
    output: process.stdout
});
r.context.ut = ut;
r.context.a = a;
r.context.a1 = a1;
